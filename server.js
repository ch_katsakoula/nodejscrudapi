import sql from "mssql";

const config = {
  user: "titika",
  password: "titika",
  server: "37.98.192.67",
  port: 4535,
  database: "FeedEm",
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
  },
  options: {
    encrypt: true, // for azure
    trustServerCertificate: true, // change to true for local dev / self-signed certs
  },
};

let databaseConnect = () => {
  sql.connect(config, function (err) {
    if (err) console.log(err);
  });
};

let getReq = async (query) => {
  var request = new sql.Request();
  request.query(query, function (err, recordset) {
    if (err) return err;
    return recordset;
  });
};

export { databaseConnect, getReq };
