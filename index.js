import express from "express";
import usersRoutes from "./routes/users.routes.js";
import {databaseConnect} from "./server.js";

const app = express();
const PORT = 5000;
app.use(express.json());

//routes
app.use("/users", usersRoutes);
app.use("/", (req, res) => {
  res.sendStatus(200);
});

app.listen(PORT, () => {
  databaseConnect();
  console.log(`Servers running on port: http://localhost:${PORT}`);
});
